﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour {

	PlayerInventory playerInv;
    //animation du carac
    Animation animations;
    //speed
    public float walkSpeed;
    public float runSpeed;
    public float turnSpeed;
	//attaque
	public float attackCooldown;
	public bool isAttacking;
	public float currentCooldown;
	public float attackRange;
	public GameObject rayhit;
    //inputs
    public string inputFront;
    public string inputBack;
    public string inputLeft;
    public string inputRight;
    public Vector3 jumpSpeed;
    CapsuleCollider playerCollider;
	public bool isDead = false;

	public float healCost;
	public GameObject healGO;
	public float healSpeed;
	Transform Target;
	GameObject theSpell;
	public bool isHealing;
	public float currentHealCooldown;
	public float HealCooldown;
    // Use this for initialization
    void Start () {
        animations = gameObject.GetComponent<Animation>();
        playerCollider = gameObject.GetComponent<CapsuleCollider>();
		playerInv = gameObject.GetComponent<PlayerInventory>();
		rayhit = GameObject.Find ("RayHit");
	}
	bool IsGrounded()
    {
		return Physics.CheckCapsule(playerCollider.bounds.center, new Vector3(playerCollider.bounds.center.x, playerCollider.bounds.min.y - 0.1f, playerCollider.bounds.center.z), 0.08f, layermask:3);
    }
	// Update is called once per frame
	void Update () {
		if (!isDead) {


        if (Input.GetKey(inputFront) && !Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(0,0, walkSpeed * Time.deltaTime);
				if (!isAttacking)
				{
					animations.Play("walk");
				}

				if (Input.GetKeyDown(KeyCode.Mouse0))
				{
					Attack();
				}
        }
        if (Input.GetKey(inputFront) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(0, 0, runSpeed * Time.deltaTime);
            animations.Play("run");
        }
        if (Input.GetKey(inputBack))
        {
            transform.Translate(0, 0, -(walkSpeed / 2) * Time.deltaTime);
				if (!isAttacking)
				{
					animations.Play("walk");
				}

				if (Input.GetKeyDown(KeyCode.Mouse0))
				{
					Attack();
				}
        }
        if (Input.GetKey(inputLeft))
        {
            transform.Rotate(0, -turnSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey(inputRight))
        {
            transform.Rotate(0, turnSpeed * Time.deltaTime, 0);
        }
        if (!Input.GetKey(inputFront) && !Input.GetKey(inputBack))
        {
				if (!isAttacking && !isHealing)
				{
					animations.Play("idle");
				}

				if (Input.GetKeyDown(KeyCode.Mouse0))
				{
					Attack();
				}

				if (Input.GetKeyDown(KeyCode.Mouse1))
				{
					Heal();
				}
        }
        if(Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            //preparation
            Vector3 v = gameObject.GetComponent<Rigidbody>().velocity;
            v.y = jumpSpeed.y;

            // Saut
            gameObject.GetComponent<Rigidbody>().velocity = jumpSpeed;
        	}
		}
		if (isAttacking)
		{
			currentCooldown -= Time.deltaTime;
		}

		if(currentCooldown <= 0)
		{
			currentCooldown = attackCooldown;
			isAttacking = false;
		}
		if (isHealing)
		{
			Spellposition ();
			currentHealCooldown -= Time.deltaTime;
		}

		if(currentHealCooldown <= 0)
		{
			currentHealCooldown = HealCooldown;
			isHealing = false;
			Destroy(theSpell, 0);
		}
    }
	public void Attack(){
		if (!isAttacking) {
			animations.Play ("attack");
			RaycastHit hit;
			if (Physics.Raycast (rayhit.transform.position, transform.TransformDirection (Vector3.forward), out hit, attackRange)) {
				Debug.DrawLine (rayhit.transform.position, hit.point, Color.red);
				if (hit.transform.tag == "Enemy") {
					hit.transform.GetComponent<enemyAi> ().ApplyDammage (playerInv.currentDamage);
				}
			}
		}
		isAttacking = true;

	}
	public void Heal(){
		if (!isAttacking && playerInv.currentMana>=healCost) {
			animations.Play ("attack");
			theSpell = Instantiate (healGO, rayhit.transform.position, transform.rotation);
			theSpell.transform.position = GameObject.Find("player").transform.position;
			isHealing = true;
			isAttacking = true;
		}
	}
	public void Spellposition(){
			Target = GameObject.Find("player").transform;
			theSpell.transform.position = Target.position;
	}
}